
<?php



require 'vendor/autoload.php';
//Connection à la base de données
try{
   $pdo = new PDO('sqlite:bddBot.db');
   $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
 } catch(Exception $e) {
   echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
   die();
 }

//Guzzle permet de récupérer le site web
   $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', 'https://www.pays-lac-aiguebelette.com/je-decouvre/le-patrimoine/points-de-vue-et-tables-dorientation/');
    $body = $response->getBody();
  

// Dom-crawler permet de filtrer les infos récupérées (image)
use Symfony\Component\DomCrawler\Crawler ;
$crawler = new Crawler((string) $body);
$pictures = $crawler
     ->filter('.ApidaeListingSimpleGrid-picture  > img')->extract(array('src'));
// print_r($pictures);
// inserImg($pdo,$pictures);
// inserAlt($pdo,$pictures);
$picturesAlt = $crawler
     ->filter('.ApidaeListingSimpleGrid-picture  > img')->extract(array('alt'));


// //recuperer commune
$towns = $crawler->filterXPath('//span[contains(@class, "ApidaeListingSimpleGrid-commune")]')->extract(['_text']);
// inserCommune($pdo,$town);

//recuperer titre
$titles = $crawler->filterXPath('//h2[contains(@class, "ApidaeListingSimpleGrid-title")]')->extract(['_text']);
//fonction inserer title
// inserTitles($pdo,$titles);

//recuper description
$dess = $crawler->filterXPath('//p[contains(@class, "ApidaeListingSimpleGrid-desc")]')->extract(['_text']);
//fonction inserer description
// inserLieu($pdo,$dess);






//Guzzle permet de récupérer le 2eme site web
$response = $client->request('GET', 'https://www.savoie-mont-blanc.com/Decouvrir/Explorer/Sites-naturels/Les-plus-beaux-panoramas-a-contempler');
$body = $response->getBody();

// Dom-crawler permet de filtrer les infos récupérées (image)
$crawler = new Crawler((string) $body);
$picturesTwo = $crawler
 ->filter('.s-slider__item  > img')->extract(array('src'));
// print_r($pictures);
// inserImg($pdo,$pictures);
// inserAlt($pdo,$pictures);
$picturesAltTwo = $crawler
 ->filter('.s-slider__item  > img')->extract(array('alt'));

// //recuperer commune ne marche pas a voir
$townsTwo = $crawler->filterXPath('//h2[contains(@class, "")]')->extract(['_text']);
// print_r($town);
// inserCommune($pdo,$town);

//recuperer titre
$titlesTwo = $crawler->filterXPath('//h2[contains(@class, "")]')->extract(['_text']);
//function inserer title
// inserTitles($pdo,$titles);

//recuper description
$dessTwo = $crawler->filterXPath('//div[contains(@class, "s-text--sans")]')->extract(['_text']);
//fonction inserer description
// inserLieu($pdo,$dess);




//Envoi des données collectées avec le bot dans la BD
//Remplissage de la table Restaurants (nom, adresse, telephone)
// $nbVue = count($pictures);
// for ($i = 0; $i < $nbVue; $i++){
//     $stmt = $pdo->prepare("INSERT INTO vue (photo, alt) VALUES (:photo, :alt)");
//     $stmt->bindParam(':photo', $pictures[$i]);
//     $stmt->bindParam(':alt', $picturesAlt[$i]);
//     $stmt->execute(); // Envoi les données dans la base
// }

// $nbVue = count($picturesTwo);
// for ($i = 0; $i < $nbVue; $i++){
//     $stmt = $pdo->prepare("INSERT INTO vue (photo, alt) VALUES (:photo, :alt)");
//     $stmt->bindParam(':photo', $picturesTwo[$i]);
//     $stmt->bindParam(':alt', $picturesAltTwo[$i]);
//     $stmt->execute(); // Envoi les données dans la base
// }


//recuperer ID.
// $stmt = $pdo->prepare("SELECT `ID` FROM `vue`");
// if ($stmt->execute()) {
//   while ($row = $stmt->fetchAll()) {
//    print_r($row);
//   }
// }
   

// $sth = $pdo->prepare("SELECT `ID` FROM `vue`");
// $sth->execute();
 
// /* Récupération de toutes les lignes d'un jeu de résultats */
// print("Récupération de toutes les lignes d'un jeu de résultats :\n");
// $result = $sth->fetchAll();
// print_r($result);


for ($i = 0; $i < count($pictures); ++$i) {
   $stmt = $pdo->prepare("INSERT INTO vue (photo, alt) VALUES (:photo, :alt)");
   $stmt2 = $pdo->prepare("INSERT INTO plus (titre, commune, description, ID_vue) VALUES (:titre, :commune, :description, :ID_vue)");
   $result = $stmt->execute(array(':photo' => $pictures[$i], ':alt' => $picturesAlt[$i])); $lastId = $pdo->lastInsertId();
   $result2 = $stmt2->execute(array(':titre' => $titles[$i], ':commune' => $town[$i], ':description' => $dess[$i], ':ID_vue' => $lastId));
}

for ($i = 0; $i < count($picturesTwo); ++$i) {
   $stmt = $pdo->prepare("INSERT INTO vue (photo, alt) VALUES (:photo, :alt)");
   $stmt2 = $pdo->prepare("INSERT INTO plus (titre, commune, description, ID_vue) VALUES (:titre, :commune, :description, :ID_vue)");
   $result = $stmt->execute(array(':photo' => $picturesTwo[$i], ':alt' => $picturesAltTwo[$i])); $lastId = $pdo->lastInsertId();
   $result2 = $stmt2->execute(array(':titre' => $titlesTwo[$i], ':commune' => $townTwo[$i], ':description' => $dessTwo[$i], ':ID_vue' => $lastId));
}


// $nbPlus = count($titles);
//    for ($i = 0; $i < $nbPlus; $i++){
//    $stmt = $pdo->query('SELECT `ID` FROM `vue`');
//    $rows = $stmt->fetchAll($_GET);
//    print_r($row);
// }

//Envoi des données collectées avec le bot dans la BD
// $nbPlus = count($titles);
// for ($i = 0; $i < $nbPlus; $i++){
//     $stmt = $pdo->prepare("INSERT INTO plus (titre, commune, description, ID_vue) VALUES (:titre, :commune, :description, :ID_vue)");
//     $stmt->bindParam(':titre', $titles[$i]);
//     $stmt->bindParam(':commune', $towns[$i]);
//     $stmt->bindParam(':description', $dess[$i]);
//     $stmt->bindParam(':ID_vue', $row[0]);
//     $stmt->execute(); // Envoi les données dans la base
// }

// $nbPlus = count($titlesTwo);
// for ($i = 0; $i < $nbPlus; $i++){
//     $stmt = $pdo->prepare("INSERT INTO plus (titre, commune, description, ID_vue) VALUES (:titre, :commune, :description, :ID_vue)");
//     $stmt->bindParam(':titre', $titlesTwo[$i]); 
//     $stmt->bindParam(':commune', $townsTwo[$i]);
//     $stmt->bindParam(':description', $dessTwo[$i]);
//     $stmt->bindParam(':ID_vue', $row[$i]);
//     $stmt->execute(); // Envoi les données dans la base
// }



// $requeste = $pdo->prepare("INSERT INTO plus ( ID_vue, titre, commune, Description) VALUES (:ID_vue, :titre, :commune, :description)");
// $requeste2 = $pdo->prepare("INSERT INTO vue (photo, alt) VALUES (:photo, :alt)"); // remplit la table Photos
// foreach ($pictures as $value => $picture) {
// $requeste->execute(array(':titre' => $titles[$value], ':commune' => $towns[$value],  ':description' => $dess[$value]));
// $lastId = $pdo->lastInsertId(); //Récupère l'id du dernier Etablissement inséré
// $requeste2->execute(array(':photo' => $pictures[$value], ':alt' => $picturesAlt[$value], ':ID_vue' => $lastId[$value]));
// }






//  //fonction pour inserer titles
// function inserTitles($pdo,$titles) {
//    $stmt = $pdo->prepare("INSERT INTO plus (titre) VALUES (:titre)");
//    foreach($titles as $title) {
//       $stmt->execute(array(
//          ':titre' => $title
//       ));
//    }

// }

// //fonction pour inserer description
// function inserLieu($pdo,$dess) {
//    $stmt = $pdo->prepare("INSERT INTO plus (description) VALUES (:description)");
//    foreach($dess as $des) {
//       $stmt->execute(array(
//          ':description' => $des
//       ));
//    }
// }




// //fonction pour inserer photo
// function inserImg($pdo,$pictures) {
//    $stmt = $pdo->prepare("INSERT INTO vue (photo) VALUES (:photo)");
//    foreach($pictures as $picture) {
//      $stmt->execute(array(
//          ':photo' => $picture[0]
//       ));
//    }
// }

// //fonction pour inserer photo
// function inserAlt($pdo,$pictures) {
//    $stmt = $pdo->prepare("INSERT INTO vue (alt) VALUES (:alt)");
//    foreach($pictures as $picture) {
//      $stmt->execute(array(
//          ':alt' => $picture[1]
//       ));
//    }
// }






// //fonction pour inserer commune
// function inserCommune($pdo,$towns) {
//    $stmt = $pdo->prepare("INSERT INTO plus (commune) VALUES (:commune)");
//    foreach($towns as $town) {
//       $stmt->execute(array(
//          ':commune' => $town
//       ));
//    }
// }


















// UPDATE `eleve` SET `nom` = 'Paradis', `prenom` = 'Vanessa' WHERE `eleve`.`numero` =1 ; 











?>

